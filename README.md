# Project built with Create React App

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run lint`

Launches the EsLinter

## Short explanation

### `env file`

Usually I use `.env` file to set up keys and ids, but here I use simple js file
to avoid sending / losing file via mail.

### `Tests`

I wrote few test to show which libraries I can use.

### `React Query`

I chose this technology coz it's much better than Redux:
fewer files, has refetch, has caching, error handling, ... 


