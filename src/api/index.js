import axios from 'axios';
import {weatherCoreLink} from '../env';

export const fetchData = async (url, params) => await axios.get(`${weatherCoreLink}${url}`, params);
