export const getWeatherIcon = (icon) => {
    switch (icon) {
        case '01d':
            return 'day.svg';
        case '01n':
            return 'night.svg';
        case '02d':
            return 'cloudy-day-1.svg';
        case '02n':
            return 'cloudy-night-1.svg';
        case '03d':
        case '03n':
        case '04d':
        case '04n':
        case '50d':
        case '50n':
            return 'cloudy.svg';
        case '09d':
        case '09n':
            return 'rainy-6.svg';
        case '10d':
        case '10n':
            return 'rainy-3.svg';
        case '11d':
        case '11n':
            return 'thunder.svg';
        case '13d':
        case '13n':
            return 'snowy-5.svg';

        default:
            return 'cloudy.svg';
    }
};
