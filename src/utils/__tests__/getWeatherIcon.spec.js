import {getWeatherIcon} from '../getWeatherIcon';

describe('getWeatherIcon', () => {
    it('returns day', () => {
        const icon = '01d';
        expect(getWeatherIcon(icon)).toBe('day.svg');
    });
    it('returns default value', () => {
        const icon = '';
        expect(getWeatherIcon(icon)).toBe('cloudy.svg');
    });
});
