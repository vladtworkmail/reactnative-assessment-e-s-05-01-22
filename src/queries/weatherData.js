import { fetchData } from '../api';
import { apiKey} from '../env';

export const fetchWeatherData = async ({ queryKey }) => {
    const [ , params ] = queryKey;
    const  { location: { lat, lng } } = params;
    const url  = `/weather?lat=${lat}&lon=${lng}&appid=${apiKey}&units=metric`;

    return await fetchData(url);
};
