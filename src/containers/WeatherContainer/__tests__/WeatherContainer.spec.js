import {QueryClient, QueryClientProvider} from 'react-query';
import {cleanup, render} from '@testing-library/react';
import WeatherContainer from '../index';

describe('WeatherContainer', () => {
    const queryClient = new QueryClient();
    let component;

    const renderComponent = () => {
        component =  render(
            <QueryClientProvider client={queryClient}>
                <WeatherContainer/>
            </QueryClientProvider>
        );
    };

    beforeEach(renderComponent);
    afterEach(cleanup);

    test('snapshot', () => {
        const { asFragment } = component;
        expect(asFragment()).toMatchSnapshot();
    });
});
