import React, { useState } from 'react';
import AlgoliaPlaces from 'algolia-places-react';
import { Container } from '@mui/material';
import { styled } from '@mui/material/styles';
import { appId, apiKeyAlgolia } from '../../env';
import WeatherCard from '../../components/WeatherCard';
import { useWeather } from '../../hooks/useWeather';

const RootBox = styled(Container)(() => ({
    display: 'flex',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'column',
    minHeight: 900,
    padding: 60
}));

const WeatherContainer = () => {
    const [location, setLocation] = useState({
        lat: 34.0537,
        lng: -118.243
    });
    const [city, setCity] = useState('Los Angeles');
    const [country, setCountry] = useState('United States');

    const {data, isLoading, isError} = useWeather(location);
    const handleLocationChange = (params) => {
        const {
            suggestion: {
                latlng, city, country
            }
        } = params;
        setLocation(latlng);
        setCity(city);
        setCountry(country);
    }

    return (
        <RootBox maxWidth='md'>
            <AlgoliaPlaces
                placeholder='Write an address here'
                onChange={handleLocationChange}
                options={{appId, apiKey: apiKeyAlgolia, aroundLatLngViaIP: false}}
            />
            {isLoading && <p>Loading...</p>}
            {isError && <p>Error!</p>}
            <WeatherCard
                mainWeatherParams={data?.data?.main || {}}
                weatherDescription={data?.data?.weather[0] || {}}
                wind={data?.data?.wind || {}}
                city={city}
                country={country}
            />
        </RootBox>
    );
};

export default WeatherContainer;
