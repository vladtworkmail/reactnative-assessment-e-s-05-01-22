import { render, cleanup } from '@testing-library/react';
import App from './App';
import { QueryClient, QueryClientProvider} from 'react-query';

describe('renders learn react link', () => {
  const queryClient = new QueryClient();
  let component;

  const renderComponent = () => {
    component = render(
        <QueryClientProvider client={queryClient}>
          <App />
        </QueryClientProvider>
    );
  };

  beforeEach(renderComponent);
  afterEach(cleanup);

  test('it renders', () => {
    const  {getByTestId } = component;
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(getByTestId('app_test')).toBeInTheDocument();
  });

  test('snapshot', () => {
    const { asFragment } = component;
    expect(asFragment()).toMatchSnapshot();
  });

});
