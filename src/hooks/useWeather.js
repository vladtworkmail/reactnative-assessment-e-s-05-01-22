import {useQuery} from 'react-query';
import {fetchWeatherData} from '../queries/weatherData';

export const useWeather = (location) => {
    const {data = {}, isLoading, isError} = useQuery(['weatherData', {location}], fetchWeatherData);
    return {
        data,
        isLoading,
        isError
    };
};
