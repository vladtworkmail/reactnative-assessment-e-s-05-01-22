import { QueryClient, QueryClientProvider } from 'react-query';
import { renderHook, waitFor } from '@testing-library/react';
import * as hooks from "../useWeather";

describe('useWeather', () => {
    const queryClient = new QueryClient();
    const testData = {
        data: {},
        isLoading: true,
        isError: false
    }
    const wrapper = ({ children }) => (
        <QueryClientProvider client={queryClient}>
            {children}
        </QueryClientProvider>
    );

    test('returns data', async () => {
        const { result } = renderHook(() => hooks.useWeather(), { wrapper });

        await waitFor(() => result.current.isSuccess);
        expect(result.current).toEqual(testData);
    });
});
