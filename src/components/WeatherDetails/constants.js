export const details = [
    {
        value: 'feels_like',
        label: 'Feels like'
    },
    {
        value: 'humidity',
        label: 'Humidity'
    },
    {
        value: 'pressure',
        label: 'Pressure'
    },
    {
        value: 'temp_max',
        label: 'Temp Max'
    },
    {
        value: 'temp_min',
        label: 'Temp Min'
    },

]
