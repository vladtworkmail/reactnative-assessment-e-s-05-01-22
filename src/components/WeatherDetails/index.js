import React from 'react';
import { styled } from '@mui/material/styles';
import { alpha, Box, Divider } from '@mui/material';
import { colors } from '../../constants/colors';
import { details } from './constants';
import PropTypes from 'prop-types';

const RootBox = styled(Box)(() => ({
    padding: 20,
    minWidth: 250,
    color: colors.white,
    backgroundColor: alpha(colors.blue, 0.45),
    borderRadius: 3,
    opacity: 0.95,
    fontWeight: 500,
    textAlign: 'start'
}));

const TitleText = styled('p')(() => ({
    margin: 0,
    marginBottom: 5,
    fontWeight: 600,
    fontSize: 25
}));

const TextDetailsContainer = styled(Box)(() => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
}));

const WeatherDetails = ({data}) => (
    <RootBox>
        <TitleText>Details</TitleText>
        <Divider color={colors.white}/>
        {
            details.map(({ label, value }) => (
                <TextDetailsContainer key={label}>
                    <p>{label}:</p>
                    <p>{data[value]}</p>
                </TextDetailsContainer>
            ))
        }
    </RootBox>
);
WeatherDetails.propTypes = {
    data: PropTypes.objectOf(PropTypes.number)
};

export default WeatherDetails;
