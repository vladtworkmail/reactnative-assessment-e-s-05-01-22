import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';
import WeatherIcon from '../WeatherIcon';
import { getWeatherIcon } from '../../utils/getWeatherIcon';
import WeatherDetails from '../WeatherDetails';
import { colors } from '../../constants/colors';

const StyledWeatherCard = styled(Box)(() => ({
    color: colors.white,
    display: 'flex',
    alignItems: 'flex-end',
    alignSelf: 'flex-start',
}));

const MainTemp = styled('h1')(() => ({
    fontSize: 90,
    fontWeight: 800,
    margin: 0
}));
const CityText = styled('h3')(() => ({
    textAlign: 'start',
    margin: 0,
    fontSize: 43,
    fontWeight: 500
}));
const CountryText = styled('h4')(() => ({
    margin: 0,
    textAlign: 'start',
    fontSize: 16,
    fontWeight: 400
}));

const WeatherCard = ({ mainWeatherParams, weatherDescription, city, country }) => (
    <StyledWeatherCard>
        <MainTemp>{mainWeatherParams.temp}°</MainTemp>
        <Box>
            <CityText>{city}</CityText>
            <CountryText>{country}</CountryText>
        </Box>
        <WeatherIcon
            icon={getWeatherIcon(weatherDescription.icon)}
        />
        <WeatherDetails
            data={mainWeatherParams}
        />
    </StyledWeatherCard>
);

WeatherCard.propTypes = {
    mainWeatherParams: PropTypes.objectOf(PropTypes.number),
    weatherDescription: PropTypes.shape({
        description: PropTypes.string,
        icon: PropTypes.string,
        main: PropTypes.string,
    }),
    city: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired
};
export default WeatherCard;
