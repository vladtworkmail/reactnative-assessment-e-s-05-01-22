import React from 'react';
import PropTypes from 'prop-types';

const WeatherIcon = ({ icon }) => (
    <img src={`${window.location.origin}/weather-icons/${icon}`} width={110} height={110} alt=""/>
);

WeatherIcon.propTypes = {
    icon: PropTypes.string.isRequired
};

export default WeatherIcon;
