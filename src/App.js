import './App.css';
import WeatherContainer from './containers/WeatherContainer';

function App() {
  return (
    <div className="App" data-testid="app_test">
      <WeatherContainer />
    </div>
  );
}

export default App;
